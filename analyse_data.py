from urllib.request import urlopen
import json
import pandas as pd
from pyspark.sql import SparkSession

# create sparksession Load the data
spark = SparkSession \
    .builder \
    .appName("Pysparkexample") \
    .config("spark.some.config.option", "some-value") \
    .getOrCreate()

# EXTACT DATA 
first_url = "https://content-us-1.content-cms.com/api/06b21b25-591a-4dd3-a189-197363ea3d1f/delivery/v1/search?q=classification:content&fl=document:[json]&fl=type&rows=100"  
first_url_response = urlopen(first_url)
data_json_first_url = json.loads(first_url_response.read())

second_url = "https://content-us-1.content-cms.com/api/06b21b25-591a-4dd3-a189-197363ea3d1f/delivery/v1/search?q=classification:category&rows=100"
second_url_response = urlopen(second_url)
data_json_second_url = json.loads(second_url_response.read())

# MERGE DATA
for data in data_json_second_url["documents"]:
    for data_first in data_json_first_url["documents"]:
        if "nutritionalFeatures" in data_first["document"]["elements"]:
            nf = data_first["document"]["elements"]["nutritionalFeatures"]["categories"]
            path = []
            for each in nf:
                nf = each.split("/")[1]
                if data["name"] == nf:
                    path.append(data["path"])
            if len(path) == 1:
                data_first["document"]["elements"]["category"]["path"] = str(path)
            elif len(path) == 0:
                print("No proper merge possible")
            else:
                data_first["document"]["elements"]["category"]["path"] = path

# TARNSFORM DATA
df = pd.json_normalize(data_json_first_url, 'documents')
all_dict = {}
names = []
for each in df.columns:
    name = each.split(".")
    actual_name = name[len(name)-1]
    if actual_name in names:
        actual_name = name[len(name)-2] + "_" + name[len(name)-1]
    names.append(actual_name)
    all_dict[each] = actual_name
df.rename(columns=all_dict,inplace=True)
df.to_csv("dataset.csv",index=False)

# LOAD DATA INTO SPARK RDD
if spark.version.startswith("2"):
     csv_plugin = "csv"
else:
     csv_plugin = "com.databricks.spark.csv"
dataframe = spark.read.format(csv_plugin).options(header='true', inferSchema='true').load('dataset.csv')
rdd = dataframe.rdd
print(type(rdd))

# ANALYSE DATA - Count documents belonging to path
rdd_groupby = rdd.groupBy(lambda x: x.path).distinct()
new_rdd = rdd_groupby.collect()
for item in new_rdd:
    print("Path Name:",item[0], "Count:",len(list(item[1])))
