# Coding Task
This is a coding task, which contains a user to fetch data from the respective urls and merge certain attributes, transform them and load them into a bigdata runtime like apache spark and analyse the results.

## Project Architecture
 ![Architecture Diagram](architecture_diagram.PNG)

The architecture has beeen designed in a straight foward way. We think of each step as a serviec with its own functionality. The GET_Data as we can see in the diagram would be the first step for us. We fetch all of the respective data and then try to merge the data from thr urls given. After the merging task is completed we then tranform the data into a typed table structure which is compatible with apache spark. Once these phases are done we then analyze the data by performing a small transformation/action on them. We can have a pictorial representation as to how this all would look sequentially in the above diagram.

## Project Structure
The project is composed of the following dependencies,
* [Python]()
* [ApacheSpark]()
* [JAVA]()
* [JDK8]()
* [Hadoop]()

## Environmental Variables
Please note to configure these environmental variables inorder to run the project
* [SPARK_HOME]()
* [PYSPARK_PATH]()
* [JAVA_HOME]()
* [HADOOP_HOME]()


## Source Code Structure
```
Coding_Task
	
	|-- analyse_data.py               # Source code of the application
	|-- architecture_diagram.PNG      # Diagram of the arhcitecture
```

## Error
If there are any errors incurred with installation of apache spark or of configuration, please follow the below link

```
https://spark.apache.org/

```
## Running Project
This project can be run manually or with spark application with the following options:
```
${SPARK_HOME}/bin/spark-submit <PYTHONFILE>.py
```
> SPARK_HOME would be the path where spark is configured.
